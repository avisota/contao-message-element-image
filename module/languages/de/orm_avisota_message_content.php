<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/de/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:19+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['addImage']['0']      = 'Bild hinzufügen';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['addImage']['1']      = 'Fügen Sie dem Inhaltselement ein Bild hinzu.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageAlt']['0']      = 'Alternativer Text';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageAlt']['1']      = 'Hier können Sie einen Alternativtext für das Bild (als <em>alt</em>-Attribut) festlegen.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageCaption']['0']  = 'Bildunterschrift';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageCaption']['1']  = 'Hier können Sie einen kurzen Text angeben, der als Bildunterschrift angezeigt werden soll.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageFloating']['0'] = 'Bildausrichtung';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageFloating']['1'] = 'Bitte geben Sie an, wie das Bild ausgerichtet werden soll.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageMargin']['0']   = 'Bildabstand';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageMargin']['1']   = 'Hier können Sie den Wert für einen oberen, rechten, unteren und linken Bildabstand festlegen.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSize']['0']     = 'Bildbreite und -höhe';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSize']['1']     = 'Hier können Sie die Bildgröße und die Methoden zur Größenanpassung festlegen.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSource']['0']   = 'Quelldatei';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSource']['1']   = 'Bitte wählen Sie eine Datei oder einen Ordner aus dem Dateiverzeichnis aus..';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageUrl']['0']      = 'Ziel des Bildlinks';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageUrl']['1']      = 'Ein hier festgelegtes Linkziel wird einen vorhandenen Lightbox-Link überschreiben, so dass das Bild hier nicht mehr in voller Größe aufrufbar ist.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['image_legend']       = 'Bildeinstellungen';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imglink_legend']     = 'Bildlink-Einstellungen';
