<?php

/**
 * Translations are managed using Transifex. To create a new translation
 * or to help to maintain an existing one, please register at transifex.com.
 *
 * @link    http://help.transifex.com/intro/translating.html
 * @link    https://www.transifex.com/projects/p/avisota-contao/language/rm/
 *
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 *
 * last-updated: 2014-03-25T14:18:19+01:00
 */

$GLOBALS['TL_LANG']['orm_avisota_message_content']['addImage']['0']      = 'Agiuntar in maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['addImage']['1']      = 'Agiuntar in maletg a l\'element da cuntegn.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['alt']['0']           = 'Text alternativ';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['alt']['1']           = 'Qua pos ti endatar in text alternativ per il maletg (attribut <em>alt</em>).';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['caption']['0']       = 'Legenda dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['caption']['1']       = 'Qua pos ti endatar in curt text che vegn visualisà sut il maletg.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['floating']['0']      = 'Alignament dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['floating']['1']      = 'Specifitgescha l\'alignament dal maletg.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageAlt']['0']      = 'Text alternativ';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageAlt']['1']      = 'Qua pos ti endatar in text alternativ per il maletg (attribut <em>alt</em>).';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageCaption']['0']  = 'Legenda dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageCaption']['1']  = 'Qua pos ti endatar in curt text che vegn visualisà sut il maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageFloating']['0'] = 'Alignament dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageFloating']['1'] = 'Specifitgescha l\'alignament dal maletg.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageMargin']['0']   = 'Distanza dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageMargin']['1']   = 'Qua pos ti inditgar ina distanza per sura, dretg, sut e sanester dal maletg.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSize']['0']     = 'Ladezza ed autezza dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSize']['1']     = 'Qua pos ti definir las dimensiuns dal maletg ed il modus da midar grondezza.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSource']['0']   = 'Datoteca da funtauna';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSource']['1']   = 'Tscherna ina datoteca u in ordinatur da la structura d\'ordinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageUrl']['0']      = 'Destinaziun da la colliaziun dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageUrl']['1']      = 'Ina colliaziun persunalisada che surscriva la colliaziun da lightbox, uschè ch\'il maletg na po betg pli vegnir contemplà en la versiun gronda.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['image_legend']       = 'Configuraziun dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imagemargin']['0']   = 'Distanza dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imagemargin']['1']   = 'Qua pos ti inditgar ina distanza per sura, dretg, sut e sanester dal maletg.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imglink_legend']     = 'Configuraziun da la colliaziun da maletgs';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['singleSRC']['0']     = 'Datoteca da funtauna';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['singleSRC']['1']     = 'Tscherna ina datoteca u in ordinatur da la structura d\'ordinaturs.';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['size']['0']          = 'Ladezza ed autezza dal maletg';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['size']['1']          = 'Qua pos ti definir las dimensiuns dal maletg ed il modus da midar grondezza.';
