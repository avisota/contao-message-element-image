<?php

/**
 * Avisota newsletter and mailing system
 * Copyright (C) 2013 Tristan Lins
 *
 * PHP version 5
 *
 * @copyright  bit3 UG 2013
 * @author     Tristan Lins <tristan.lins@bit3.de>
 * @package    avisota/contao-message-element-image
 * @license    LGPL-3.0+
 * @filesource
 */

/**
 * Fields
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['addImage']      = array(
    'Add an image',
    'Add an image to the content element.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSource']   = array(
    'Source file',
    'Please select a file or folder from the files directory.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageAlt']      = array(
    'Alternate text',
    'Here you can enter an alternate text for the image (<em>alt</em> attribute).'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageSize']     = array(
    'Image width and height',
    'Here you can set the image dimensions and the resize mode.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageMargin']   = array(
    'Image margin',
    'Here you can enter the top, right, bottom and left margin.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageUrl']      = array(
    'Image link target',
    'A custom image link target will override the lightbox link, so the image cannot be viewed fullsize anymore.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageFloating'] = array(
    'Image alignment',
    'Please specify how to align the image.'
);
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imageCaption']  = array(
    'Image caption',
    'Here you can enter a short text that will be displayed below the image.'
);

/**
 * Legends
 */
$GLOBALS['TL_LANG']['orm_avisota_message_content']['image_legend']   = 'Image settings';
$GLOBALS['TL_LANG']['orm_avisota_message_content']['imglink_legend'] = 'Image link settings';
